Zadanie rekrutacyjne dla Allegro

##Uruchamianie i używanie aplikacji

-otworzyć plik index.html najnowszą wersją przeglądarki Chrome/Firefox

-w formularzu wpisać wyszukiwaną nazwę użytkownika(np. allegro) i wcisnąć przycisk Search lub enter



#Uwagi

Wyszukiwana nazwa użytkownika sprawdzana jest pod kątem poprawności przez wyrażenie regularne (źródło: https://github.com/shinnn/github-username-regex).
W przypadku gdy podana nazwa użytkownika jest prawidłowa, wyświetla się tabela z listą posortowanych repozytoriów według popularności(stargazers) wraz z odnośnikiem do githuba.
W przypadku gdy wyszukiwany użytkownik nie istnieje lub nazwa jest nieprawidłowa, aplikacja zwraca komunikat błędu.
Całość aplikacji została napisana w czystym Javascripcie, bez użycia zewnętrznych bibliotek czy frameworków. Przy każdym wyszukiwaniu tabela jest czyszczona i budowana na nowo.

