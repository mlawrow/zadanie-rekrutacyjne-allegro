/**
 *     Oczekiwanie na załadowanie się zawartości DOM
 */
document.addEventListener('DOMContentLoaded', async function () {

    const searchButton = document.getElementById("realButton");
    const input = document.getElementById("inp1");
    const errp = document.getElementById("errorpar");
    const repList = document.getElementById("repList")

    //dodanie funkcjonalności do przycisku
    searchButton.addEventListener('click', searchButtonHandler);
    //podpięcie klawisza enter pod przycisk wyszukiwania
    input.addEventListener('keydown', function (event) {
        if (event.code === "Enter") {
            event.preventDefault();
            searchButton.click();
        }
    });

    //funkcja obsługująca przycisk
    function searchButtonHandler() {
        //sprawdzenie czy wpisana nazwa użytkownika jest prawidłowa
        errp.innerHTML = " ";
        repList.innerHTML = "";
        let regEx = /^[a-z\d](?:[a-z\d]|-(?=[a-z\d])){0,38}$/i;
        if (regEx.test(input.value)) {
            //zapytanie do api githuba o dane
            let file = "https://api.github.com/users/" + input.value + "/repos";
            getText(file);

            async function getText(file) {
                try {
                    let response = await fetch(file);
                    if (!response.ok) {
                        throw new Error(response.status);
                    }
                    let myText = await response.text();
                    let repoList = JSON.parse(myText);
                    //sortowanie zwróconych danych po ilości gwiazdek
                    repoList = repoList.sort(function (a, b) {
                        return b["stargazers_count"] - a["stargazers_count"];
                    });
                    buildResultTable(repoList);
                } catch (e) {
                    errp.innerHTML = "Nie ma takiego użytkownika";

                }
            }
        } else {
            //obsługa błędu
            errp.innerHTML = "Nieprawidłowa nazwa użytkownika";
        }


    }

    /**
     * funkcja budująca tabelę z listą repozytoriów
     * @param repoList
     */
    function buildResultTable(repoList) {
        let txt = "<table id='repos'><th>Name</th><th>Stars</th><th>Forks</th>"
        for (let x in repoList) {
            txt += "<tr><td><a href= " + repoList[x].html_url + " target='_blank'>" + repoList[x].name + "</a></td>";
            txt += "<td>" + repoList[x].stargazers_count + "</td>";
            txt += "<td>" + repoList[x].forks + "</td></tr>";

        }
        txt += "</table>"
        repList.innerHTML = txt;
    }


});